import "./HomePage.css"
import "bootstrap/dist/css/bootstrap.min.css"
import MyModal from "./MyModal";
import Form from "./Form";
import ButtonAdd from "./ButtonAdd";
import TableOfContent from "./TableOfContents";
import { useState } from "react";

function HomePage() {
    const [modal, setModal] = useState({
        title: "",
        body: ""
    });
    const [data, setData] = useState([]);
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function addNewData(payload) {
        console.log(payload);
        setData((prevsValue) => {
            return [...prevsValue, payload]
        })
    }

    return (
        <div class="position-absolute top-50 start-50 translate-middle vstack gap-3">
            <div class="container" >
                <div class="row">
                    <div class="col"></div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <p class="card-title" className="custom-txt"><b>Form</b></p>
                                <p class="card-text">
                                    <Form modal={modal} setModal={setModal} show={show} onHide={handleClose} onShow={handleShow} data={data} addFromHome={addNewData} /></p>

                            </div>
                        </div>
                    </div>
                    <div class="col"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col"></div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <p class="card-title" className="custom-txt"><b>Table Of Contents</b></p>
                                <TableOfContent data={data} />
                            </div>
                        </div>
                    </div>
                    <div class="col"></div>
                </div>
            </div>
            <MyModal modal={modal} show={show} onHide={handleClose} />
        </div>


    );

}
export default HomePage;