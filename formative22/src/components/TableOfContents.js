function TableOfContent(props) {

    return (
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Username</th>
                    <th scope="col">Age</th>
                </tr>
            </thead>
            <tbody>
                {
                    props.data.map((value, index) => {
                        return (
                            <tr key={index}>
                                <th scope="row">{index + 1}</th>
                                <td>{value.username}</td>
                                <td>{value.age}</td>
                            </tr>
                        );
                    })
                }

            </tbody>
        </table>
    );
}
export default TableOfContent;