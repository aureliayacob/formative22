import ButtonAdd from "./ButtonAdd";
import { useState } from "react";

function Form(props) {

    function setMessage(title, body) {
        props.setModal({
            ...props.modal,
            title,
            body: body
        })
    }

    const [data, setData] = useState({
        username: "",
        age: 0,
    });

    async function nameHandler(events) { //react
        await setData((prevsValue) => {
            return {
                ...prevsValue,
                username: events.target.value
            }
        })
        await console.log(data.username);
    }

    function ageHandler(events) {
        setData((prevsValue) => {
            return {
                ...prevsValue,
                age: events.target.value
            }

        })

    }

    return (
        <div>
            <form>
                <div class="mb-3">
                    <label for="InputUsername" class="form-label">Username {data.username}</label>
                    <input type="text" class="form-control" onChange={nameHandler} />
                </div>
                <div class="mb-3">
                    <label for="InputAge" class="form-label">Age {data.age}</label>
                    <input type="text" class="form-control" onChange={ageHandler} />
                </div>
            </form>

            <ButtonAdd setMessage={setMessage} show={props.show} onShow={props.onShow} addFromForm={props.addFromHome} submitData={data} />
        </div>
    );
}
export default Form;