import { useState } from "react";
import MyModal from "./MyModal";
import "./Button.css"


function ButtonAdd(props) {
    const [show, setShow] = useState(false);
    function addData() {
        console.log(props.submitData.username);
        if (props.submitData.username === "" && props.submitData.age < 1) {
            props.setMessage("Input Invalid", "Field Tidak Boleh Kosong");
            props.onShow();
        } else if (props.submitData.username === "" && props.submitData.age > 1) {
            props.setMessage("Input Invalid", "Nama Tidak Boleh Kosong");
            props.onShow();
        } else if (props.submitData.age < 1) {
            props.setMessage("Umur Invalid", "Umur Harus Lebih Dari 0");
            props.onShow();
        } else {
            console.log(props.addFromForm);
            props.addFromForm(props.submitData)
        }

    }
    return (
        <div >
            <button type="button" className="custom-btn" onClick={addData}>Add</button>
        </div>

    );
}
export default ButtonAdd;