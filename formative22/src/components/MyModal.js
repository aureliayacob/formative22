import Modal from 'react-bootstrap/Modal';
import { useState } from "react";
import Button from 'react-bootstrap/Button';
import { propTypes } from 'react-bootstrap/esm/Image';

function MyModal(props) {

    return (
        <Modal show={props.show} onHide={props.onHide}>
            <Modal.Header closeButton onClick={props.onHide}>
                <Modal.Title>{props.modal.title}</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <p>{props.modal.body}</p>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={props.onHide}>Close</Button>

            </Modal.Footer>
        </Modal>
    );
}
export default MyModal;